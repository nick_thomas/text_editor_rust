#![warn(clippy::all,clippy::pedantic,clippy::restriction)]
#![allow(
    clippy::clippy::missing_docs_in_private_items,
    clippy::clippy::implicit_return,
    clippy::clippy::clippy::shadow_reuse,
    clippy::clippy::print_stdout,
    clippy::wildcard_enum_match_arm,
    clippy::else_if_without_else
)]
mod document;
mod editor;
mod highlighting;
mod terminal;
mod row;
use editor::Editor;

pub use document::Document;
pub use editor::Position;
pub use editor::SearchDirection;
pub use row::Row;
pub use terminal::Terminal;
fn main() {
    Editor::default().run();
}
